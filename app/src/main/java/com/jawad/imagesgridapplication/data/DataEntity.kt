package com.jawad.imagesgridapplication.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * The class DataEntity
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

@Parcelize
class DataEntity(
    @SerializedName("copyright") var copyright: String,
    @SerializedName("date") var date: String,
    @SerializedName("explanation") var explanation: String,
    @SerializedName("hdurl") var hdurl: String,
    @SerializedName("media_type") var media_type: String,
    @SerializedName("service_version") var service_version: String,
    @SerializedName("title") var title: String,
    @SerializedName("url") var url: String
) : Parcelable