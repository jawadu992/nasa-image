package com.jawad.imagesgridapplication.utils

/**
 * The class Constants
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

class Constants {
    companion object INSTANCE {
        const val POSITION = "position"
        const val SPLASH_DELAY = 3000
    }
}