package com.jawad.imagesgridapplication.ui.component.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.data.DataEntity
import com.jawad.imagesgridapplication.ui.base.listeners.RecyclerItemListener

/**
 * The class RVGridAdapter
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

class RVGridAdapter constructor(
    private val onItemClickListener: RecyclerItemListener,
    private val dataList:Array<DataEntity>): RecyclerView.Adapter<ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
         val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_image_data,
            parent, false
        )
        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(dataList[position], onItemClickListener)
    }
}