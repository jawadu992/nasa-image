package com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.imageDisplay

import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.ui.base.BaseFragment
import com.jawad.imagesgridapplication.ui.component.ViewModelFactory
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.ImageDetailViewModel
import com.jawad.imagesgridapplication.utils.Constants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_image_display.view.*
import javax.inject.Inject

/**
 * The ImageDisplayFragment.
 */
class ImageDisplayFragment : BaseFragment() {

    @Inject
    lateinit var imageDetailViewModel: ImageDetailViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override val layoutId: Int
        get() = R.layout.fragment_image_display

    override fun initializeDagger() {
        imageDetailViewModel = (viewModelFactory).create(imageDetailViewModel::class.java)
    }

    override fun initializePresenter(view: View) {
        imageDetailViewModel.loadImageData(arguments!!.getInt(Constants.POSITION, 0))
        init(imageDetailViewModel, view)
    }

    /**
     * Observer data change in the ImageDetailViewModel
     * If DataEntity has data at the given position
     * UI will set, data to the UI from observer on data change.
     * @param imageDisplayFragment
     * @param view
     */
    private fun init(imageDisplayFragment: ImageDetailViewModel, view: View) {
        imageDisplayFragment.url.observe(this, Observer {
            val url = it
            view.ib_share.setOnClickListener {
                shareInformation(view.tv_title.text.toString(), url)
            }
            Picasso.get()
                .load(it)
                .resize(1000,1000)
                .centerInside()
                .placeholder(R.drawable.nasalogo)
                .into(view.iv_photo)
        })
        imageDisplayFragment.title.observe(this, Observer {
            view.tv_title.text = it
        })
        imageDisplayFragment.date.observe(this, Observer {
            view.tv_date.text = it
        })
        imageDisplayFragment.description.observe(this, Observer {
            view.tv_description.text = it
        })
        imageDisplayFragment.copyright.observe(this, Observer {
            view.tv_copyright.text = it
        })
    }

    /**
     * To share content across multiple channels, including email,
     * text messaging, social networking and more
     *
     * @param title
     * @param subject, content to share the data
     */
    private fun shareInformation(title: String, subject: String) {
        val shareText: String = title + System.lineSeparator() + System.lineSeparator() + subject
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, title)
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        startActivity(Intent.createChooser(intent, "Share via"))
    }
}
