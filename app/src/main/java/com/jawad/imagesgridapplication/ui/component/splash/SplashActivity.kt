package com.jawad.imagesgridapplication.ui.component.splash

import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.ui.base.BaseActivity
import com.jawad.imagesgridapplication.ui.component.ViewModelFactory
import com.jawad.imagesgridapplication.ui.component.home.MainActivity
import com.jawad.imagesgridapplication.utils.Constants
import javax.inject.Inject
import org.jetbrains.anko.startActivity


class SplashActivity : BaseActivity(){

    @Inject
    lateinit var splashViewModel: SplashViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override val layoutId: Int
        get() = R.layout.activity_splash

    /**
     * Initialize the ViewModel class for the SplashActivity class
     */
    override fun initializeViewModel() {
        splashViewModel = viewModelFactory.create(splashViewModel::class.java)
    }

    /**
     *  To show Splash UI in Full screen
     *  1) Disabled Title
     *  2) Window Flags is set to
     *      WindowManager.LayoutParams.FLAG_FULLSCREEN and
     *      WindowManager.LayoutParams.FLAG_FULLSCREEN
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)
        navigateToHomeScreen()
    }

    /**
     * Navigates to Home Screen after three sec's
     */
    private fun navigateToHomeScreen() {
        Handler().postDelayed({
            startActivity<MainActivity>()
            finish()
        }, Constants.SPLASH_DELAY.toLong())
    }
}
