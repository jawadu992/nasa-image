package com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.jawad.imagesgridapplication.App
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.imageDisplay.ImageDisplayFragment
import com.jawad.imagesgridapplication.utils.Constants

/**
 * The class ViewPagerFragmentAdapter
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 *
 * Created new ImageDisplayfragment for each page and set bundled current page position.
 *
 */

class ViewPagerFragmentAdapter constructor(
    fm: FragmentManager
) :
    FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        val imageDisplayFragment = ImageDisplayFragment()
        val imageDisplayBundle = Bundle()
        imageDisplayBundle.putInt(Constants.POSITION, position)
        imageDisplayFragment.arguments = imageDisplayBundle
        return imageDisplayFragment
    }

    override fun getCount(): Int {
        return App.getDataEntity()!!.size
    }
}