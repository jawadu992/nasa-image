package com.jawad.imagesgridapplication.ui.component.splash

import com.jawad.imagesgridapplication.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * The class SplashViewModel
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

class SplashViewModel  @Inject constructor(): BaseViewModel()