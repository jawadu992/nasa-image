package com.jawad.imagesgridapplication.ui.base

import androidx.lifecycle.ViewModel

/**
 * The class BaseViewModel
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

abstract class BaseViewModel: ViewModel()