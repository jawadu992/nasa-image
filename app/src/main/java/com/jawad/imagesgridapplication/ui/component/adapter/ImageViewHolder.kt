package com.jawad.imagesgridapplication.ui.component.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.data.DataEntity
import com.jawad.imagesgridapplication.ui.base.listeners.RecyclerItemListener
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_image_data.*


/**
 * The class ImageViewHolder
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

class ImageViewHolder(override val containerView: View?) :
    RecyclerView.ViewHolder(containerView!!), LayoutContainer {

    /**
     * Called when onBindViewHolder is triggered in RVGridAdapter
     * The new bind will be used to set the values to display items
     * @param dataEntity contains the values to set in UI
     * @param onItemClickListener pass the click item position
     */
    fun bind(
        dataEntity: DataEntity,
        onItemClickListener: RecyclerItemListener
    ) {
        Picasso.get().load(dataEntity.url).placeholder(R.drawable.nasalogo)
            .error(R.drawable.nasalogo)
            .resize(400,400)
            .centerCrop()
            .into(iv_photo)
        itemView.setOnClickListener {
            onItemClickListener.onItemSelected(adapterPosition)
        }
    }
}