package com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.imageDisplay

import com.jawad.imagesgridapplication.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * The class ImageDetailViewModel
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

@Suppress("SENSELESS_COMPARISON")
class ImageDisplayViewModel @Inject constructor() : BaseViewModel() {

}