package com.jawad.imagesgridapplication.ui.component.imageDetailScreen

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.ui.base.BaseActivity
import com.jawad.imagesgridapplication.ui.component.ViewModelFactory
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.ViewPagerFragmentAdapter
import com.jawad.imagesgridapplication.utils.Constants
import kotlinx.android.synthetic.main.activity_image_detail.*
import javax.inject.Inject

class ImageDetailActivity : BaseActivity() {

    @Inject
    lateinit var imageDetailViewModel: ImageDetailViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override val layoutId: Int
        get() = R.layout.activity_image_detail

    /**
     * Initialize the ViewModel class for the ImageDetailActivity class
     */
    override fun initializeViewModel() {
        imageDetailViewModel = viewModelFactory.create(imageDetailViewModel::class.java)
    }

    /**
     *  To show image in Full screen
     *  1) Disabled Title
     *  2) Window Flags is set to
     *      WindowManager.LayoutParams.FLAG_FULLSCREEN and
     *      WindowManager.LayoutParams.FLAG_FULLSCREEN
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)
        iv_close.setOnClickListener {
           onBackPressed()
        }
        initializeView()
    }

    /**
     * Initialize all the view
     * Setting view pager adapter
     * Set the currently page by image index
     * Default page index will set to 0
     */
    private fun initializeView() {
        vp_image.adapter = ViewPagerFragmentAdapter(supportFragmentManager)
        vp_image.currentItem = intent.getIntExtra(Constants.POSITION, 0)
    }
}
