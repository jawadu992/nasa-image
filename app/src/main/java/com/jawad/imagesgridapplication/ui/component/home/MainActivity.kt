package com.jawad.imagesgridapplication.ui.component.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.jawad.imagesgridapplication.R
import com.jawad.imagesgridapplication.ui.base.BaseActivity
import com.jawad.imagesgridapplication.ui.base.listeners.RecyclerItemListener
import com.jawad.imagesgridapplication.ui.component.ViewModelFactory
import com.jawad.imagesgridapplication.ui.component.adapter.RVGridAdapter
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.ImageDetailActivity
import com.jawad.imagesgridapplication.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject


class MainActivity : BaseActivity(), RecyclerItemListener {

    @Inject
    lateinit var mainViewModel: MainViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override val layoutId: Int
        get() = R.layout.activity_main

    /**
     * Initialize the ViewModel class for the MainActivity class
     */
    override fun initializeViewModel() {
        mainViewModel = viewModelFactory.create(mainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeImageListView()
        mainViewModel.loadJson()
        onDataLoad(mainViewModel)
    }

    /**
     * Initialize the image list view
     */
    private fun initializeImageListView() {
        val staggeredGridLayoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        rv_item.layoutManager = staggeredGridLayoutManager
    }

    /**
     * onDataLoad observer data change in the MainViewModel
     * If LiveData has data
     * Set, it will be delivered to the UI from observer (on data change).
     *
     * @param mainViewModel
     */
    private fun onDataLoad(mainViewModel: MainViewModel) {
        mainViewModel.dataEntity.observe(this, Observer {
            rv_item.adapter = RVGridAdapter(this, it)
            tv_image_count.text = it.size.toString()
        })
    }

    /**
     * Call ImageDetailActivity on item click
     * @param position of the clicked item
     */
    override fun onItemSelected(position: Int) {
        startActivity<ImageDetailActivity>(Constants.POSITION to position)
    }
}
