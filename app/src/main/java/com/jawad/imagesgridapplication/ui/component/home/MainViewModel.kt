package com.jawad.imagesgridapplication.ui.component.home

import androidx.lifecycle.MutableLiveData
import com.jawad.imagesgridapplication.App
import com.jawad.imagesgridapplication.data.DataEntity
import com.jawad.imagesgridapplication.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * The class MainViewModel
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

@Suppress("SENSELESS_COMPARISON")
class MainViewModel @Inject constructor() : BaseViewModel() {
    var dataEntity: MutableLiveData<Array<DataEntity>> = MutableLiveData()

    /**
     * Loads the data list from the application class
     * dataEntity wil contain the array of DataEntity
     * @see #onDataLoad() function in MainActivity class
     */
    fun loadJson() {
        dataEntity.postValue(App.getDataEntity())
    }
}