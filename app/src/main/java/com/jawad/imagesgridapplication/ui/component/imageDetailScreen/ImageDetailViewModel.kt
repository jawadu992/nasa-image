package com.jawad.imagesgridapplication.ui.component.imageDetailScreen

import androidx.lifecycle.MutableLiveData
import com.jawad.imagesgridapplication.App
import com.jawad.imagesgridapplication.data.DataEntity
import com.jawad.imagesgridapplication.ui.base.BaseViewModel
import javax.inject.Inject

/**
 * The class ImageDetailViewModel
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */
@Suppress("SENSELESS_COMPARISON")
class ImageDetailViewModel @Inject constructor() : BaseViewModel() {
    var title: MutableLiveData<String> = MutableLiveData()
    var date: MutableLiveData<String> = MutableLiveData()
    var description: MutableLiveData<String> = MutableLiveData()
    var copyright: MutableLiveData<String> = MutableLiveData()
    var url: MutableLiveData<String> = MutableLiveData()

    /**
     * Load the data from Application class for the given position
     * Each params data will be set at the UI
     * @param position
     * @see init method in ImageDisplayFragment
     */
    fun loadImageData(position: Int) {
        val dataEntity: DataEntity = App.getDataEntity()!![position]
        title.postValue(dataEntity.title)
        date.postValue(dataEntity.date)
        description.postValue(dataEntity.explanation)
        url.postValue(dataEntity.hdurl)
        if (dataEntity.copyright != null && dataEntity.copyright.isNotEmpty())
            copyright.postValue("Copyright © ${dataEntity.copyright}")
    }

}