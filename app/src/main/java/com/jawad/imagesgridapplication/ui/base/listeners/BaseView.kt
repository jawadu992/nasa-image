package com.jawad.imagesgridapplication.ui.base.listeners

/**
 * The class BaseView
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */
interface BaseView