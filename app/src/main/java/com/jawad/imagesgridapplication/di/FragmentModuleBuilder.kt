package com.jawad.imagesgridapplication.di

import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.imageDisplay.ImageDisplayFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * The class FragmentModuleBuilder
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */


@Suppress("unused")
@Module
abstract class FragmentModuleBuilder{
    @ContributesAndroidInjector
    abstract fun contributeImageDetailFragment(): ImageDisplayFragment
}