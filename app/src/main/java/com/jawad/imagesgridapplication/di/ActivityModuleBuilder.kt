package com.jawad.imagesgridapplication.di

import com.jawad.imagesgridapplication.ui.component.home.MainActivity
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.ImageDetailActivity
import com.jawad.imagesgridapplication.ui.component.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * The class ActivityModuleBuilder
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

@Suppress("unused")
@Module
abstract class ActivityModuleBuilder{
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
    @ContributesAndroidInjector
    abstract fun contributeImageDetailActivity(): ImageDetailActivity
}