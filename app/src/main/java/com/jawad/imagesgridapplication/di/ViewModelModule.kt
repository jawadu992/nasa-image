package com.jawad.imagesgridapplication.di

import androidx.lifecycle.ViewModel
import com.jawad.imagesgridapplication.ui.component.home.MainViewModel
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.ImageDetailViewModel
import com.jawad.imagesgridapplication.ui.component.imageDetailScreen.fragment.imageDisplay.ImageDisplayViewModel
import com.jawad.imagesgridapplication.ui.component.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * The class ViewModelModule
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

@Suppress("unused")
@Module
abstract class ViewModelModule{
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailViewModel::class)
    abstract fun bindImageDetailViewModel(viewModel: ImageDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImageDisplayViewModel::class)
    abstract fun bindImageDisplayViewModel(viewModel: ImageDisplayViewModel): ViewModel
}