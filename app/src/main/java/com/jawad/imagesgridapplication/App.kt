package com.jawad.imagesgridapplication

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jawad.imagesgridapplication.data.DataEntity
import com.jawad.imagesgridapplication.di.DaggerAppComponent
import com.jawad.imagesgridapplication.utils.Calligraphy.CalligraphyConfig
import com.squareup.picasso.BuildConfig
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import java.io.IOException
import java.lang.ref.WeakReference
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

/**
 * The class App
 *
 * @author  Jawad Usman
 * @web www.jawadusman.com
 * @version 1.0
 * @since 25 Feb 2020
 */

class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var dispatchingAndroidFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        app = WeakReference(this)

        val jsonFileString = getJsonFromAssets()
        val gson = Gson()
        val listPersonType = object : TypeToken<Array<DataEntity>>() {}.type
        dataEntityList = gson.fromJson(jsonFileString, listPersonType)
        dataEntityList.sortByDescending(dateTimeStrToLocalDateTime)

        DaggerAppComponent.builder().application(this).build().inject(this)
        if (BuildConfig.DEBUG)
            Picasso.get().setIndicatorsEnabled(true)
        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/gilroyMedium.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun supportFragmentInjector(): AndroidInjector<Fragment> =
        dispatchingAndroidFragmentInjector

    /**
     * Create a convert function, String to LocalDate
     **/
    private val dateTimeStrToLocalDateTime: (DataEntity) -> LocalDate = {
        LocalDate.parse(it.date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
    }

    /**
     * Return the Data String
     * Read the Json File from your assets file
     *
     * @returns String JSON data, or empty string if it does not pass any data
     */
    private fun getJsonFromAssets(): String {
        return try {
            assets.open("data.json").bufferedReader().use { it.readText() }
        } catch (e: IOException) {
            ""
        }
    }

    companion object {
        private lateinit var app: WeakReference<App>
        private lateinit var dataEntityList: Array<DataEntity>

        fun getDataEntity(): Array<DataEntity>? {
            return dataEntityList
        }

        fun getInstance(): App? {
            return app.get()
        }
    }
}